(ns a3.core
  (:gen-class))

(def data_set
  [{:date "March 7, 1970"
    :location "Eastern United States"
    :type "Total Solar Eclipse"
    :significance "First Total Solar Eclipse Visible from Continental United States Since 1925"}
   {:date "June 30, 1973"
    :location "Africa, Middle East, India"
    :type "Total Solar Eclipse"
    :significance "First Total Solar Eclipse Visible from Africa Since 1961"}
   {:date "August 21, 2017"
    :location "United States"
    :type "Total Solar Eclipse"
    :significance "Great American Eclipse, Visible across Continental United States"}
   {:date "July 11, 1991"
    :location "Hawaii, Mexico, Central America, South America"
    :type "Total Solar Eclipse"
    :significance "Longest Total Solar Eclipse of the 20th Century"}
   {:date "July 2, 2019"
    :location "South Pacific, South America"
    :type "Total Solar Eclipse"
    :significance "Path of Totality Crosses Chile and Argentina"}])

(defn prompt [s]
  (print s)
  (flush)
  (read-line))

(defn print_1_element [element]
  (println (str "Date: " (:date element)))
  (println (str "Location: " (:location element)))
  (println (str "Type: " (:type element)))
  (println (str "Significance: " (:significance element)))
  (println "-------------------------------------------------"))

(defn view_eclipse_events []
  (println "")
  (println "-------------------------------------------------")
  (doseq [value (map-indexed vector data_set)]
    (println (str "Index: " (first value))) (print_1_element (nth value 1)))
  (println ""))

(defn add_eclipse_event []
  (def date (re-matches #"^(January|February|March|April|May|June|July|August|September|October|November|December) [0-3]?[0-9], [0-9]+$" (prompt "Please enter the date: ")))
  (def location (re-matches #"[a-zA-Z]+,?" (prompt "Please enter the location: ")))
  (def type (prompt "Please enter the type: "))
  (def significance (prompt "Please enter the significance: "))
  (if (not (or (nil? date) (nil? location)))
    (do (def event {:date (first date) :location location :type type :significance significance})
        (def data_set (conj data_set event))
        (println "")
        (println "Event added successfully")
        (println ""))
    (println "error")))

(defn modify_eclipse_event []
  (def index_to_be_modified (Integer/parseInt (prompt "What is the index of the item you want modified: ")))
  (println (count data_set))
  (if (< index_to_be_modified (count data_set))
    (do
      (def date (re-matches #"^(January|February|March|April|May|June|July|August|September|October|November|December) [0-3]?[0-9], [0-9]+$" (prompt "Please enter the new date: ")))
      (def location (re-matches #"[a-zA-Z]+,?" (prompt "Please enter the new location: ")))
      (def type (prompt "Please enter the new type: "))
      (def significance (prompt "Please enter the new significance: "))
      (if (not (or (nil? date) (nil? location)))
        (do (def event {:date (first date) :location location :type type :significance significance})
            (def data_set (assoc data_set index_to_be_modified event))
            (println "")
            (println "Event added successfully")
            (println ""))
        (println "error")))
    (println "Index out of bounds")))

(defn search_eclipse_event []
  (def element_to_search (prompt "Enter the search query: "))
  (println "")
  (println "-------------------------------------------------")
  (doseq [value (map-indexed vector data_set)]
    (if  (not (nil? (re-find (re-pattern (clojure.string/lower-case element_to_search)) (clojure.string/lower-case (:location (nth value 1))))))
      (do (println (str "Index: " (first value))) (print_1_element (nth value 1)))))
  (println ""))

(defn print_main_menu []
  (println "=== Eclipse History Encyclopedia ===")
  (println "1. View Eclipse Events")
  (println "2. Add New Eclipse Event")
  (println "3. Modify Eclipse Event")
  (println "4. Search for Eclipse Events")
  (println "5. Exit")
  (println "")
  (print "Enter your choice: ")
  (flush))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (loop [a 0]
    (print_main_menu))
  (def i (Integer/parseInt (read-line)))
  (cond
    (= i 1) (do (view_eclipse_events) (recur 1))
    (= i 2) (do (add_eclipse_event) (recur 1))
    (= i 3) (do (modify_eclipse_event) (recur 1))
    (= i 4) (do (search_eclipse_event) (recur 1))
    (= i 5) ()
    :else (do (println "Wrong input") (recur 1))))

